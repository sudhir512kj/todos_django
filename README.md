# Todos API

## About

This is a simple todos API.

## Features

With this API;

- You can create, view, update, and delete a todo

## Technology stack

Tools used during the development of this API are;

- [Django](https://www.djangoproject.com) - a python web framework
- [Django REST Framework](http://www.django-rest-framework.org) - a flexible toolkit to build web APIs
- [SQLite](https://www.sqlite.org/) - this is a database server

## Requirements

- Use Python 3.x.x+
- Use Django 2.x.x+

## Running the application

To run this application, clone the repository on your local machine and execute the following command.

```sh
    $ cd todos_skm
    $ virtualenv --python=python3 venv
    $ source virtenv/bin/activate
    $ pip install -r requirements.txt
    $ python manage.py makemigrations
    $ python manage.py migrate
    $ python manage.py runserver
```
