from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Todos(models.Model):
    # song title
    title = models.CharField(max_length=255,	null=False)
    content = models.TextField()
    upload = models.FileField(upload_to='uploads/%Y/%m/%d/')
    done = models.BooleanField(null=False)
    user = models.IntegerField()

    def __str__(self):
        return self.title
