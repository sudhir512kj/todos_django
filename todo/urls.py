from django.urls import path
from .views import TodosList, TodoDetail, LoginView, RegisterUsers


urlpatterns = [
    path('todos/', TodosList.as_view(), name="todos-list-create"),
    path('todos/<int:pk>/', TodoDetail.as_view(), name="todos-detail"),
    path('auth/login/', LoginView.as_view(), name="auth-login"),
    path('auth/register/', RegisterUsers.as_view(), name="auth-register")
]
