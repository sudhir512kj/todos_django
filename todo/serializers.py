from rest_framework import serializers
from django.contrib.auth.models import User
from . import models


class TodosSerializer(serializers.ModelSerializer):

    class Meta:
        fields = ('id', 'title', 'content', 'done',)
        model = models.Todos

    def update(self, instance, validated_data):
        instance.title = validated_data.get("title", instance.title)
        instance.content = validated_data.get("content", instance.content)
        instance.done = validated_data.get("done", instance.done)
        instance.save()
        return instance


class TokenSerializer(serializers.Serializer):
    """
    This serializer serializes the token data
    """

    token = serializers.CharField(max_length=255)


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("username", "email")
