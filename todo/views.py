from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login

from rest_framework import generics
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import status
from rest_framework_jwt.settings import api_settings

from .decorators import validate_request_data
from .models import Todos
from .serializers import TodosSerializer, TokenSerializer, UserSerializer

# Get the JWT settings, add these lines after the import/from lines
jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


class TodosList(generics.ListCreateAPIView):
    queryset = Todos.objects.all()
    serializer_class = TodosSerializer
    permission_classes = (permissions.IsAuthenticated,)

    @validate_request_data
    def post(self, request, *args, **kwargs):
        a_todo = Todos.objects.create(
            title=request.data["title"],
            content=request.data["content"],
            upload=request.data["upload"],
            done=request.data["done"],
            user=request.session["_auth_user_id"]
        )
        return Response(
            data=TodosSerializer(a_todo).data,
            status=status.HTTP_201_CREATED
        )


class TodoDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Todos.objects.all()
    serializer_class = TodosSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        try:
            a_todo = self.queryset.get(
                pk=kwargs["pk"], user=request.session["_auth_user_id"])
            return Response(TodosSerializer(a_todo).data)
        except Todos.DoesNotExist:
            return Response(
                data={
                    "message": "Todo with id: {} does not exist".format(kwargs["pk"])
                },
                status=status.HTTP_404_NOT_FOUND
            )

    @validate_request_data
    def put(self, request, *args, **kwargs):
        try:
            a_todo = self.queryset.get(pk=kwargs["pk"])
            serializer = TodosSerializer()
            updated_todo = serializer.update(a_todo, request.data)
            return Response(TodosSerializer(updated_todo).data)
        except Todos.DoesNotExist:
            return Response(
                data={
                    "message": "Todo with id: {} does not exist".format(kwargs["pk"])
                },
                status=status.HTTP_404_NOT_FOUND
            )

    def delete(self, request, *args, **kwargs):
        try:
            a_todo = self.queryset.get(pk=kwargs["pk"])
            a_todo.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Todos.DoesNotExist:
            return Response(
                data={
                    "message": "Todo with id: {} does not exist".format(kwargs["pk"])
                },
                status=status.HTTP_404_NOT_FOUND
            )


class LoginView(generics.CreateAPIView):
    """
    POST auth/login/
    """
    # This permission class will overide the global permission
    # class setting
    permission_classes = (permissions.AllowAny,)

    queryset = User.objects.all()

    def post(self, request, *args, **kwargs):
        username = request.data.get("username", "")
        password = request.data.get("password", "")
        user = authenticate(request, username=username, password=password)
        if user is not None:
            # login saves the user’s ID in the session,
            # using Django’s session framework.
            login(request, user)
            serializer = TokenSerializer(data={
                # using drf jwt utility functions to generate a token
                "token": jwt_encode_handler(
                    jwt_payload_handler(user)
                )})
            serializer.is_valid()
            return Response(serializer.data)

        return Response(status=status.HTTP_401_UNAUTHORIZED)


class RegisterUsers(generics.CreateAPIView):
    """
    POST auth/register/
    """
    permission_classes = (permissions.AllowAny,)

    def post(self, request, *args, **kwargs):
        username = request.data.get("username", "")
        password = request.data.get("password", "")
        email = request.data.get("email", "")
        if not username and not password and not email:
            return Response(
                data={
                    "message": "username, password and email is required to register a user"
                },
                status=status.HTTP_400_BAD_REQUEST
            )
        new_user = User.objects.create_user(
            username=username, password=password, email=email
        )
        return Response(
            data=UserSerializer(new_user).data,
            status=status.HTTP_201_CREATED
        )
