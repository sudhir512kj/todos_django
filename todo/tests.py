import json
from django.urls import reverse
from django.contrib.auth.models import User

from rest_framework.test import APITestCase, APIClient
from rest_framework.views import status
from .models import Todos
from .serializers import TodosSerializer


class TodosModelTest(APITestCase):
    def setUp(self):
        self.a_todo = Todos.objects.create(
            title="Ugandan anthem",
            content="George William Kakoma",
            done=False
        )

    def test_todo(self):
        """"
        This test ensures that the todo created in the setup
        exists
        """
        self.assertEqual(self.a_todo.title, "Ugandan anthem")
        self.assertEqual(self.a_todo.content, "George William Kakoma")
        self.assertEqual(str(self.a_todo),
                         "Ugandan anthem")


# tests for views


class BaseViewTest(APITestCase):
    client = APIClient()

    @staticmethod
    def create_todo(title="", content="", done=False):
        if title != "" and content != "":
            Todos.objects.create(title=title, content=content, done=done)

    def make_a_request(self, kind="post", **kwargs):
        """
        Make a post request to create a todo
        :param kind: HTTP VERB
        :return:
        """
        if kind == "post":
            return self.client.post(
                reverse(
                    "todos-list-create"
                ),
                data=json.dumps(kwargs["data"]),
                content_type='application/json'
            )
        elif kind == "put":
            return self.client.put(
                reverse(
                    "todos-detail",
                    kwargs={
                        "pk": kwargs["id"]
                    }
                ),
                data=json.dumps(kwargs["data"]),
                content_type='application/json'
            )
        else:
            return None

    def fetch_a_todo(self, pk=0):
        return self.client.get(
            reverse(
                "todos-detail",
                kwargs={
                    "pk": pk
                }
            )
        )

    def delete_a_todo(self, pk=0):
        return self.client.delete(
            reverse(
                "todos-detail",
                kwargs={
                    "pk": pk
                }
            )
        )

    def login_a_user(self, username="", password=""):
        url = reverse(
            "auth-login"
        )
        return self.client.post(
            url,
            data=json.dumps({
                "username": username,
                "password": password
            }),
            content_type="application/json"
        )

    def login_client(self, username="", password=""):
        # get a token from DRF
        response = self.client.post(
            reverse("create-token"),
            data=json.dumps(
                {
                    'username': username,
                    'password': password
                }
            ),
            content_type='application/json'
        )
        self.token = response.data['token']
        # set the token in the header
        self.client.credentials(
            HTTP_AUTHORIZATION='Bearer ' + self.token
        )
        self.client.login(username=username, password=password)
        return self.token

    def register_a_user(self, username="", password="", email=""):
        return self.client.post(
            reverse(
                "auth-register"
            ),
            data=json.dumps(
                {
                    "username": username,
                    "password": password,
                    "email": email
                }
            ),
            content_type='application/json'
        )

    def setUp(self):
        # create a admin user
        self.user = User.objects.create_superuser(
            username="test_user",
            email="test@mail.com",
            password="testing",
            first_name="test",
            last_name="user",
        )
        # add test data
        self.create_todo("like glue", "what happened", False)
        self.create_todo("simple song", "what happened", False)
        self.create_todo("love is wicked", "what happened", False)
        self.create_todo("jam rock", "what happened", False)
        self.valid_data = {
            "title": "test todo",
            "content": "test content"
            "done": True
        }
        self.invalid_data = {
            "title": "",
            "artist": ""
        }
        self.valid_todo_id = 1
        self.invalid_todo_id = 100


class GetAllTodosTest(BaseViewTest):

    def test_get_all_todos(self):
        """
        This test ensures that all songs added in the setUp method
        exist when we make a GET request to the songs/ endpoint
        """
        # hit the API endpoint
        response = self.client.get(
            reverse("todos-all")
        )
        # fetch the data from db
        expected = Todos.objects.all()
        serialized = TodosSerializer(expected, many=True)
        self.assertEqual(response.data, serialized.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class AuthLoginUserTest(BaseViewTest):
    """
    Tests for the auth/login/ endpoint
    """

    def test_login_user_with_valid_credentials(self):
        # test login with valid credentials
        response = self.login_a_user("test_user", "testing")
        # assert token key exists
        self.assertIn("token", response.data)
        # assert status code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # test login with invalid credentials
        response = self.login_a_user("anonymous", "pass")

        # assert status code is 401 UNAUTHORIZED
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
